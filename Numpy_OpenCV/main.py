#Could represent image as list of 3 lists, however inefficient with memory
import numpy
#Create array 0 to 26, 1 increments - 1D array
arrayA = numpy.arange(27)
print(arrayA)
print(type(arrayA))

print(arrayA.reshape(3,9))
# print(arrayA) #does not keep stage unless assigned

print(arrayA.reshape(3,3,3)    )
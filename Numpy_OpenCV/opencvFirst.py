import numpy
import cv2

#Convert image to numpy array using CV2
#imports as single value per pixel (grayscale)
im_cathedral = cv2.imread("cathedral.jpeg", 0)
print(im_cathedral)
cv2.imwrite("test.jpg", im_cathedral)

#Use 1 for a 3 dimensional array
#3 Arrays, 1 for each RGB of a pixel
#imports as color
im_cathedral3D = cv2.imread("cathedral.jpeg", 1)
# print(im_cathedral3D)

#Write array to image
# cv2.imwrite("test.jpg", im_cathedral3D)

a = [1,2,3]
# print(a[0:2])
# extract subsection of array
print(im_cathedral3D[0:2,2:4])

#Iterate through array
    #Print off line by line
# for i in im_cathedral.T:
    # print(i)

    #Print off one at a time
# for i in im_cathedral.flat:
#     print(i)
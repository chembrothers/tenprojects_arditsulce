import numpy
import cv2
im_cathedral = cv2.imread("cathedral.jpeg", 0)

#stack arrays on top of each other
#horizontal stack
# ims = numpy.hstack((im_cathedral, im_cathedral)) #2 positional argument, need 1
ims = numpy.vstack((im_cathedral, im_cathedral)) #2 positional argument, need 1
cv2.imwrite("test.jpg", ims)

#split into smaller arrays - needs equal division
lst = numpy.hsplit(im_cathedral, 4)
print(lst)